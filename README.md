# ics-ans-role-proxmox-sflow

Ansible role to install proxmox-sflow exporter.

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## Role Variables

```yaml
hsflow_polling: 20
hsflow_sampling: 1000

# collector IP
hsflow_collector_ip: 10.1.1.1

# collector listening port - sflow port
hsflow_collector_port: 6343

# hsflow interfaces
# can be dev=<DEV>, where <DEV> can be:
# vmbr0, etc.
# speed = 1G-1T polls all interfaces
hsflow_pcap_interface: "speed=1G-1T"
hsflow_agent_ip_selection: "{{ ansible_default_ipv4.network }}/{{ (ansible_default_ipv4.address + '/' + ansible_default_ipv4.netmask) | ipaddr('prefix') }}"

# ESS debian repository
hsflow_artifactory_de_repo: "https://artifactory.esss.lu.se/artifactory/list/debian-mirror/"
hsflow_debian_version: stretch
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-proxmox-sflow
```

## License

BSD 2-clause
